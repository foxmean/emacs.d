(require 'package)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)


;;Foxmean's Configuration
;;=======================
;;(set-language-environment "Thai")
;;(set-language-environment "UTF-8")
(set-input-method 'thai-kesmanee)
(toggle-input-method)
(setq visible-bell 1) ;; turn off bell
(setq inhibit-startup-screen t)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(menu-bar-mode 0)
(global-linum-mode 0)
(load-theme 'gruvbox t)
(global-subword-mode 1) ;; move by SubWord
(electric-pair-mode 1) ;; auto insert closing bracketaaa
(delete-selection-mode 1) ;; make typing delete/overwrite selected text
(show-paren-mode 1) ;; turn on bracket match highlight
(save-place-mode 1) ;; remember cursor position
(global-auto-revert-mode 1) ;; when a file is update outside emacs, make it update if it's already opened in emacs
(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'list-buffers 'ibuffer) ;; make ibuffer default
;; (ido-mode 1) ;; enable ido-mode
;; (setq ido-enable-flex-matching t) ;; show any name that has the chars you typed
(global-visual-line-mode 1);; wrap long lines by word boundary, and arrow up/down move by visual line, etc


;;org-mode setting
;;================

;org-tree-slide-mode
(require 'org-tree-slide)
(global-set-key (kbd "<f8>") 'org-tree-slide-mode)
(global-set-key (kbd "S-<f8>") 'org-tree-slide-skip-done-toggle)
(define-key org-tree-slide-mode-map (kbd "<f9>")
    'org-tree-slide-move-previous-tree)
(define-key org-tree-slide-mode-map (kbd "<f10>")
    'org-tree-slide-move-next-tree)
(define-key org-tree-slide-mode-map (kbd "<f11>")
    'org-tree-slide-content)

(global-set-key (kbd "<f6>") (lambda ()
       (interactive) (setq cursor-type (if cursor-type 'nil 'box))))
(global-set-key (kbd "<f7>") 'hl-line-mode)

(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))) ;; enable org-bullets

(add-to-list 'auto-mode-alist '("\\.txt\\'" . org-mode)) ;; open *.txt file automatically with org-mode
(setq org-confirm-babel-evaluate nil) ;; stop emacs asking for confirmation evaluate code in org-mode
(setq org-src-fontify-natively t) ;; make org mode syntax color embeded source code, put the following in your emacs init


;; full screen on start
;; (defun fullscreen ()
;;        (interactive)
;;        (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
;;                  '(2 "_NET_WM_STATE_FULLSCREEN" 0)))
;; (fullscreen)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("3eb2b5607b41ad8a6da75fe04d5f92a46d1b9a95a202e3f5369e2cdefb7aac5c" "5dc0ae2d193460de979a463b907b4b2c6d2c9c4657b2e9e66b8898d2592e3de5" "98cc377af705c0f2133bb6d340bf0becd08944a588804ee655809da5d8140de6" "2d16a5d1921feb826a6a9b344837c1ab3910f9636022fa6dc1577948694b7d84" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" default)))
 '(package-selected-packages
   (quote
    (mastodon material-theme ess evil babel color-theme-solarized orgtbl-ascii-plot epresent ereader org-beautify-theme org-tree-slide org-present nov org-bullets arduino-mode markdown-mode+ auctex auctex-latexmk auctex-lua lua-mode pdf-tools powerline magit gruvbox-theme exwm)))
 '(send-mail-function (quote mailclient-send-it)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
