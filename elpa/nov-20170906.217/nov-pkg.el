;;; -*- no-byte-compile: t -*-
(define-package "nov" "20170906.217" "Featureful EPUB reader mode" '((dash "2.12.0") (esxml "0.3.3") (emacs "24.4")) :commit "38ecc9da07aeda8117e3bd744b6bf77327bf4254" :url "https://github.com/wasamasa/nov.el" :keywords '("hypermedia" "multimedia" "epub"))
