;ELC   
;;; Compiled
;;; in Emacs version 25.1.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\306\307\310\311\312\313%\210\314\315\316\317\320\321\312\307&\207" [require dash esxml esxml-query shr url-parse custom-declare-group nov nil "EPUB reader mode" :group multimedia custom-declare-variable nov-unzip-program (executable-find "unzip") "Path to `unzip` executable." :type (file :must-match t)] 8)
#@57 Temporary directory containing the buffer's EPUB files.
(defvar nov-temp-dir nil (#$ . 797))
(make-variable-buffer-local 'nov-temp-dir)
#@38 Path of the EPUB buffer's .opf file.
(defvar nov-content-file nil (#$ . 939))
(make-variable-buffer-local 'nov-content-file)
#@36 Version string of the EPUB buffer.
(defvar nov-epub-version nil (#$ . 1070))
(make-variable-buffer-local 'nov-epub-version)
#@30 Metadata of the EPUB buffer.
(defvar nov-metadata nil (#$ . 1200))
(make-variable-buffer-local 'nov-metadata)
#@98 Alist for the EPUB buffer's documents.
Each alist item consists of the identifier and full path.
(defvar nov-documents nil (#$ . 1316))
(make-variable-buffer-local 'nov-documents)
#@62 Index of the currently rendered document in the EPUB buffer.
(defvar nov-documents-index 0 (#$ . 1502))
(make-variable-buffer-local 'nov-documents-index)
#@36 TOC identifier of the EPUB buffer.
(defvar nov-toc-id nil (#$ . 1662))
(make-variable-buffer-local 'nov-toc-id)
#@40 Create a path from DIRECTORY and FILE.
(defalias 'nov-make-path #[(directory file) "\302!	P\207" [directory file file-name-as-directory] 2 (#$ . 1780)])
#@54 Non-nil if DIRECTORY contains exactly one directory.
(defalias 'nov-contains-nested-directory-p #[(directory) "\303\304\305#\211@	G\306U\205 \307\n!\205 \n*\207" [directory files file directory-files t "^[^.]" 1 file-directory-p] 5 (#$ . 1940)])
#@59 Move contents of CHILD into DIRECTORY, then delete CHILD.
(defalias 'nov-unnest-directory #[(directory child) "\304\305\306#\307\211\203 \n@\310	\"\210\nA\211\204\f *\311!\207" [child item --dolist-tail-- directory directory-files t "^[^.]" nil rename-file delete-directory] 5 (#$ . 2197)])
#@94 Extract FILENAME into DIRECTORY.
Unnecessary nesting is removed with `nov-unnest-directory'.
(defalias 'nov-unzip-epub #[(directory filename) "\305\306\211\307\310	\n&\306\311	!\211\203 \312	\"\210\202 \f*\207" [nov-unzip-program directory filename child status call-process nil t "-d" nov-contains-nested-directory-p nov-unnest-directory] 8 (#$ . 2502)])
#@44 Like `ignore-errors', but for file errors.
(defalias 'nov-ignore-file-errors '(macro . #[(&rest body) "\301\302\303B\304BBB\207" [body condition-case nil progn ((file-error nil))] 4 (#$ . 2872)]))
#@34 Return the contents of FILENAME.
(defalias 'nov-slurp #[(filename) "\302\303!rq\210\304\216\305	!\210\306 +\207" [#1=#:temp-buffer filename generate-new-buffer " *temp*" #[nil "\301!\205	 \302!\207" [#1# buffer-name kill-buffer] 2] insert-file-contents buffer-string] 2 (#$ . 3076)])
#@60 Return t if DIRECTORY contains a valid EPUB mimetype file.
(defalias 'nov-mimetype-valid-p #[(directory) "\3021 \303\304\"\305	!\306\232)0\207\210\307\207" [directory filename (file-error) nov-make-path "mimetype" nov-slurp "application/epub+zip" nil] 3 (#$ . 3370)])
#@46 Return the container filename for DIRECTORY.
(defalias 'nov-container-filename #[(directory) "\302\303\"\302	\304\")\207" [directory filename nov-make-path "META-INF" "container.xml"] 3 (#$ . 3647)])
#@42 Return the content filename for CONTENT.
(defalias 'nov-container-content-filename #[(content) "\303\304	\"\305\306\n\"*\207" [query content node "container>rootfiles>rootfile[media-type='application/oebps-package+xml']" esxml-query esxml-node-attribute full-path] 3 (#$ . 3855)])
#@53 Return t if DIRECTORY holds a valid EPUB container.
(defalias 'nov-container-valid-p #[(directory) "\304!\211\205' \305	!\205' \306\307	!!\310\n!\n\205& \205& \305\311\"!*)\207" [directory filename content content-file nov-container-filename file-exists-p xml-to-esxml nov-slurp nov-container-content-filename nov-make-path] 5 (#$ . 4145)])
#@55 Return t if DIRECTORY makes up a valid EPUB document.
(defalias 'nov-epub-valid-p #[(directory) "\301!\205	 \302!\207" [directory nov-mimetype-valid-p nov-container-valid-p] 2 (#$ . 4499)])
#@38 Return the EPUB version for CONTENT.
(defalias 'nov-content-version #[(content) "\303\304\"\305\302	\"\211\204 \306\307!\210\n*\207" [content node version esxml-query "package" esxml-node-attribute error "Version not specified"] 4 (#$ . 4697)])
#@105 Return the UUID name for CONTENT.
This is used in `nov-content-unique-identifier' to retrieve the
UUID.
(defalias 'nov-content-unique-identifier-name #[(content) "\303\304\"\305\306	\"\211\204 \307\310!\210\n*\207" [content node name esxml-query "package[unique-identifier]" esxml-node-attribute unique-identifier error "Unique identifier name not specified"] 4 (#$ . 4953)])
#@30 Return the UUID for CONTENT.
(defalias 'nov-content-unique-identifier #[(content) "\304!\305\306\307	!\"\310\311\n\"!@\211\204 \312\313	\"\210\314\315\316#+\207" [content name selector id nov-content-unique-identifier-name format "package>metadata>identifier[id='%s']" regexp-quote esxml-node-children esxml-query error "Unique identifier not found by its name: %s" replace-regexp-in-string "^urn:uuid:" ""] 5 (#$ . 5339)])
#@57 Required metadata tags used for `nov-content-metadata'.
(defvar nov-required-metadata-tags '(title language) (#$ . 5776))
#@57 Optional metadata tags used for 'nov-content-metadata'.
(defvar nov-optional-metadata-tags '(contributor coverage creator date description format publisher relation rights source subject type) (#$ . 5904))
#@167 Return a metadata alist for CONTENT.
Required keys are 'identifier and everything in
`nov-required-metadata-tags', optional keys are in
`nov-optional-metadata-tags'.
(defalias 'nov-content-metadata #[(content) "\306!\307\310\311\312\"\"\307\313\"\307\314\"\315\301	BC\f#,\207" [content identifier candidates nov-required-metadata-tags required nov-optional-metadata-tags nov-content-unique-identifier mapcar #[(node) "\301!\302!@B\207" [node esxml-node-tag esxml-node-children] 3] esxml-query-all "package>metadata>*" #[(tag) "	\236A\211\204 \303\304\"\210\n)B\207" [tag candidates candidate message "Required metadatum %s not found"] 4] #[(tag) "\211	\236AB\207" [tag candidates] 3] append optional] 5 (#$ . 6117)])
#@120 Extract an alist of manifest files for CONTENT in DIRECTORY.
Each alist item consists of the identifier and full path.
(defalias 'nov-content-manifest #[(directory content) "\301\302\303\304\"\"\207" [content mapcar #[(node) "\305!\306\302	\"A\306\303	\"A\307\n!\310\f\"+B\207" [node #1=#:--dash-source-0-- id href directory esxml-node-attributes assoc intern nov-make-path] 4] esxml-query-all "package>manifest>item"] 5 (#$ . 6860)])
#@50 Extract a list of spine identifiers for CONTENT.
(defalias 'nov-content-spine #[(content) "\301\302\303\304\"\"\207" [content mapcar #[(node) "\301\302\303\"!\207" [node intern esxml-node-attribute idref] 4] esxml-query-all "package>spine>itemref"] 5 (#$ . 7307)])
(defalias 'nov--content-epub2-files #[(content manifest files) "\306\307\"\310\311	\"\211\204 \312\313!\210\314\n!\211\f\236\211\204\" \312\315!\210+B\207" [content node id nov-toc-id manifest toc-file esxml-query "package>spine[toc]" esxml-node-attribute toc error "EPUB 2 NCX ID not found" intern "EPUB 2 NCX file not found" files] 4])
(defalias 'nov--content-epub3-files #[(content manifest files) "\306\307\"\310\302	\"\211\204 \311\312!\210\313\n!\211\f\236\211\204\" \311\314!\210\315\316\211\203R @\211@=\204C B)TA\211\2040 *\237)+B\207" [content node id nov-toc-id manifest toc-file esxml-query "package>manifest>item[properties=nav]" esxml-node-attribute error "EPUB 3 <nav> ID not found" intern "EPUB 3 <nav> file not found" nil 0 #1=#:result files it-index #2=#:list it] 4])
#@121 Create correctly ordered file alist for CONTENT in DIRECTORY.
Each alist item consists of the identifier and full path.
(defalias 'nov-content-files #[(directory content) "\306	\"\307	!\310\311\"\312\313\"\203 \314	\n\f#\202\" \315	\n\f#+\207" [directory content manifest spine files nov-epub-version nov-content-manifest nov-content-spine mapcar #[(item) "	\236\207" [item manifest] 2] version< "3.0" nov--content-epub2-files nov--content-epub3-files] 4 (#$ . 8416)])
(defalias 'nov--walk-ncx-node #[(node depth) "\306!\307\310!\311\211\203) @\306\f!\312=\203 \f	B)\nTA\211\204 *	\237)\211\313=\203C \314c\210\315\316\"\210\317c\202\231 \312=\205\231 \320\321\"\320\322\"\323\324\" \310!@! \204m \325\326!\210\327\330 !\206x  #\"\203\222 \327\331\"\"c\210\315\332\"\210\327\333!c\202\230 \327\334\"\"c-*\207" [node #1=#:result it-index #2=#:list it children esxml-node-tag nil esxml-node-children 0 navPoint navMap "<ol>\n" mapc #[(node) "\302	T\"\207" [node depth nov--walk-ncx-node] 3] "</ol>\n" esxml-query "navLabel>text" "content" esxml-node-attribute src error "Navigation point is missing href attribute" format "<a href=\"%s\">%s</a>" "<li>\n%s\n<ol>\n" #[(node) "\302	T\"\207" [node depth nov--walk-ncx-node] 3] "</ol>\n</li>\n" "<li>\n%s\n</li>\n" tag label-node content-node href label link] 5])
#@39 Convert NCX document at PATH to HTML.
(defalias 'nov-ncx-to-html #[(path) "\303\304\305\306!!\"\307\310!r\nq\210\311\216\312	\313\"\210\314 ,\207" [path root #1=#:temp-buffer esxml-query "navMap" xml-to-esxml nov-slurp generate-new-buffer " *temp*" #[nil "\301!\205	 \302!\207" [#1# buffer-name kill-buffer] 2] nov--walk-ncx-node 0 buffer-string] 5 (#$ . 9780)])
(defvar nov-mode-map (byte-code "\301 \302\303\304#\210\302\305\306#\210\302\307\310#\210\302\311\312#\210\302\313\314#\210\302\315\316#\210\302\317\320#\210\302\321\322#\210\302\323\324#\210\302\325\322#\210\302\326\327#\210\302\330\331#\210\302\332\331#\210\302\333\334#\210\302\335\336#\210\302\337\336#\210\302\340\341#\210\302\342\343#\210)\207" [map make-sparse-keymap define-key "g" nov-render-document "v" nov-view-source "V" nov-view-content-source "m" nov-display-metadata "n" nov-next-chapter "p" nov-previous-chapter "t" nov-goto-toc "" nov-browse-url [follow-link] mouse-face [mouse-2] "	" shr-next-link [134217737] shr-previous-link [backtab] " " nov-scroll-up [33554464] nov-scroll-down "" [home] beginning-of-buffer [end] end-of-buffer] 4))
#@52 Delete temporary files of all current EPUB buffer.
(defalias 'nov-clean-up #[nil "\205 \3011 \302\303\"0\207\210\304\207" [nov-temp-dir (file-error) delete-directory t nil] 3 (#$ . 10933)])
#@52 Delete temporary files of all opened EPUB buffers.
(defalias 'nov-clean-up-all #[nil "\303 \304\211\205\" 	@rq\210\n\305=\203 \306 \210)	A\211\204	 \304*\207" [buffer --dolist-tail-- major-mode buffer-list nil nov-mode nov-clean-up] 3 (#$ . 11133)])
#@49 Return t if URL refers to an external document.
(defalias 'nov-external-url-p #[(url) "\303!\211\304H\n>\204 \305\306\300	D\"\210	\307H)\205 \310\207" [url cl-x cl-struct-url-tags url-generic-parse-url 0 signal wrong-type-argument 1 t] 5 (#$ . 11395)])
#@45 Return a list of URL's filename and target.
(defalias 'nov-url-filename-and-target #[(url) "\302!\211\303H	>\204 \304\305\300D\"\210\306H\303H	>\204% \304\305\300D\"\210\307HD\207" [url cl-struct-url-tags url-generic-parse-url 0 signal wrong-type-argument 6 7] 6 (#$ . 11658)])
#@97 Insert an image for PATH at point.
This function honors `shr-max-image-proportion' if possible.
(defalias 'nov-insert-image #[(path) "\303\304!\203/ \305\306p!!\307\310	\311\312\313\314\315\316\n\3178@Z_!\320\316\n\3218A@Z_!&	!)\207\307\310	\312\211\313\314%!\207" [edges path shr-max-image-proportion fboundp imagemagick-types window-inside-pixel-edges get-buffer-window insert-image create-image imagemagick nil :ascent 100 :max-width truncate 2 :max-height 3] 14 (#$ . 11950)])
#@122 Custom <img> rendering function for DOM.
Uses `shr-tag-img' for external paths and `nov-insert-image' for
internal ones.
(defalias 'nov-render-img #[(dom) "\302A@\236A\303	!\203 \304!\202 \305	!\306	!)\207" [dom url src nov-external-url-p shr-tag-img expand-file-name nov-insert-image] 2 (#$ . 12443)])
#@124 Custom <title> rendering function for DOM.
Sets `header-line-format' to a combination of the EPUB title and
chapter title.
(defalias 'nov-render-title #[(dom) "\303\236A\305	!@\n\204 \306\204 \307\310\nE\211*\207" [nov-metadata dom chapter-title title header-line-format esxml-node-children (:propertize "No title" face italic) (:propertize "No title" face italic) ": "] 3 (#$ . 12759)])
#@61 Alist of rendering functions used with `shr-render-region'.
(defvar nov-rendering-functions '((img . nov-render-img) (title . nov-render-title)) (#$ . 13163))
#@228 Render the document referenced by `nov-documents-index'.
If the document path refers to an image (as determined by
`image-type-file-name-regexps'), an image is inserted, otherwise
the HTML is rendered with `shr-render-region'.
(defalias 'nov-render-document #[nil "	H\211@\nA\306\307\310\211\203S \203S @\203. \306\202D @\f\306\307\311#)\266\203\203D )TA\211\204 +)\312\f!\306\313 \210\203o \314\f!\210\202\213 \315\316\"\203\206 =\203\206 \317\f!c\210\202\213 \320\f!c\210\204\236  \321ed\"\210*eb.\207" [nov-documents nov-documents-index document id path #1=#:needle nil t 0 string-match file-name-directory erase-buffer nov-insert-image version< "3.0" nov-ncx-to-html nov-slurp shr-render-region image-type-file-name-regexps it-index #2=#:continue #3=#:list it inhibit-changing-match-data imagep default-directory buffer-read-only nov-epub-version nov-toc-id nov-mode-map nov-rendering-functions shr-external-rendering-functions shr-map] 8 (#$ . 13329) nil])
#@61 Return first item in `nov-documents' PREDICATE is true for.
(defalias 'nov-find-document #[(predicate) "\304\305\204 	\nGW\203 \n	H!\203 \306	T\202 \205% 	S*\207" [done i nov-documents predicate 0 nil t] 3 (#$ . 14362)])
#@50 Go to the TOC index and render the TOC document.
(defalias 'nov-goto-toc #[nil "\302\303!\211\204\f \304\305!\210\306 )\207" [index nov-documents-index nov-find-document #[(doc) "@	=\207" [doc nov-toc-id] 2] error "Couldn't locate TOC" nov-render-document] 3 (#$ . 14600) nil])
#@58 View the source of the current document in a new buffer.
(defalias 'nov-view-source #[nil "\302	HA!\207" [nov-documents nov-documents-index find-file] 3 (#$ . 14888) nil])
#@54 View the source of the content file in a new buffer.
(defalias 'nov-view-content-source #[nil "\301!\207" [nov-content-file find-file] 2 (#$ . 15067) nil])
#@57 View the metadata of the EPUB document in a new buffer.
(defalias 'nov-display-metadata #[nil "\306	r\307\f!q\210\310 \210\311\312 \210\313\314\n\"c\210\311\211\203{ @\211\211\211A\242\313\315\316\317!!\"c\210\203g \320=\203` `c\210\321`\"\210)\202n c\210\202n \322\323\324\325#c\210\326c\210+A\211\204$ *eb\210*\327\f!+\207" [nov-metadata nov-epub-version version metadata buffer buffer-read-only "*EPUB metadata*" get-buffer-create special-mode nil erase-buffer format "EPUB Version: %s\n" "%s: " capitalize symbol-name description shr-render-region propertize "None" face italic "\n" display-buffer item --dolist-tail-- #1=#:--dash-source-1-- key value beg] 6 (#$ . 15230) nil])
#@49 Go to the next chapter and render its document.
(defalias 'nov-next-chapter #[nil "	GSW\205 T\302 \207" [nov-documents-index nov-documents nov-render-document] 2 (#$ . 15960) nil])
#@53 Go to the previous chapter and render its document.
(defalias 'nov-previous-chapter #[nil "\301V\205 S\302 \207" [nov-documents-index 0 nov-render-document] 2 (#$ . 16151) nil])
#@61 Scroll with `scroll-up' or visit next chapter if at bottom.
(defalias 'nov-scroll-up #[(arg) "\301 dY\203\n \302 \207\303!\207" [arg window-end nov-next-chapter scroll-up] 2 (#$ . 16339) "P"])
#@64 Scroll with `scroll-down' or visit previous chapter if at top.
(defalias 'nov-scroll-down #[(arg) "\302 eX\203 \303V\203 \304 \210db\207\305	!\207" [nov-documents-index arg window-start 0 nov-previous-chapter scroll-down] 2 (#$ . 16539) "P"])
#@57 Visit the document as specified by FILENAME and TARGET.
(defalias 'nov-visit-relative-file #[(filename target) "	HA\306\n!\307\310\f\"!\311\312!\211\204 \313\314!\210\315 \210)\205G \316e\317\"\211\204; \313\320!\210b\210\321\322]S!),\207" [nov-documents nov-documents-index current-path directory filename path file-name-directory file-truename nov-make-path nov-find-document #[(doc) "\302	A\"\207" [path doc string-suffix-p] 3] error "Couldn't locate document" nov-render-document next-single-property-change shr-target-id "Couldn't locate target" recenter 1 index target pos scroll-margin] 5 (#$ . 16791)])
#@101 Follow an external url with `browse-url'.
Internal URLs are visited with `nov-visit-relative-file'.
(defalias 'nov-browse-url #[(&optional mouse-event) "\302!\210\303`\304\"\211\204 \305\306!\210\307	!\203 \310	!\202# \311\312\313	!\")\207" [mouse-event url mouse-set-point get-text-property shr-url user-error "No link under point" nov-external-url-p browse-url apply nov-visit-relative-file nov-url-filename-and-target] 5 (#$ . 17433) (list last-nonmenu-event)])
#@161 Hook run after entering EPUB mode.
No problems result if this variable is not bound.
`add-hook' automatically binds it.  (This is true for all hook variables.)
(defvar nov-mode-hook nil (#$ . 17909))
(byte-code "\300\301!\204\f \302\301\303\304#\210\300\207" [boundp nov-mode-map put definition-name nov-mode] 4)
(defvar nov-mode-map (make-sparse-keymap))
(byte-code "\300\301N\204 \302\300\301\303\304!#\210\305\306!\204 \302\306\307\310#\210\300\207" [nov-mode-map variable-documentation put purecopy "Keymap for `nov-mode'." boundp nov-mode-syntax-table definition-name nov-mode] 5)
(defvar nov-mode-syntax-table (make-syntax-table))
(byte-code "\300\301N\204 \302\300\301\303\304!#\210\302\305\306\307#\207" [nov-mode-syntax-table variable-documentation put purecopy "Syntax table for `nov-mode'." nov-mode-abbrev-table definition-name nov-mode] 5)
(defvar nov-mode-abbrev-table (progn (define-abbrev-table 'nov-mode-abbrev-table nil) nov-mode-abbrev-table))
(byte-code "\300\301N\204 \302\300\301\303\304!#\210\302\305\306\307#\207" [nov-mode-abbrev-table variable-documentation put purecopy "Abbrev table for `nov-mode'." nov-mode derived-mode-parent special-mode] 5)
#@211 Major mode for reading EPUB documents

In addition to any hooks its parent mode `special-mode' might have run,
this mode runs the hook `nov-mode-hook', as the final step
during initialization.

\{nov-mode-map}
(defalias 'nov-mode #[nil "\306\300!\210\307\310 \210\311\312\310\313N\203 \314\311\313\310\313N#\210\315!\204' \316\317 \"\210\320\f!\211\2036 \321 =\203< \322\f\323 \"\210)\324:\325\"\204V :;=\204V \326:\325;C#\210\327!\210\330\f!\210:;\331\332\333\334\307$\210\331\335\336\"\210\331\337\333\334\307$\210<\204~ \340\341!\210\342\343\307\344#=\345=<\"\211>\250\204\233 \333 \210\340\346>\"\210>\347U\204\253 \333 \210\340\350>\"\210)\351=!\204\272 \333 \210\340\352!\210\353\354\355=!!!?\356=\357?!\"@\360@!A\353\354@!!?@B\361?!C\362?!D\363\364\365A?\"\"E\347F,\307G\366\334\307\"\210\367 \210)\370\371!\207" [delay-mode-hooks major-mode mode-name nov-mode-map nov-mode-syntax-table parent make-local-variable t special-mode nov-mode "EPUB" mode-class put keymap-parent set-keymap-parent current-local-map char-table-parent standard-syntax-table set-char-table-parent syntax-table abbrev-table-get :parents abbrev-table-put use-local-map set-syntax-table add-hook kill-buffer-hook nov-clean-up nil kill-emacs-hook nov-clean-up-all change-major-mode-hook error "EPUB must be associated with file" make-temp-file "nov-" ".epub" nov-unzip-epub "EPUB extraction aborted by signal %s" 0 "EPUB extraction failed with exit code %d" nov-epub-valid-p "Invalid EPUB file" xml-to-esxml nov-slurp nov-container-filename nov-make-path nov-container-content-filename file-name-directory nov-content-version nov-content-metadata apply vector nov-content-files set-visited-file-name nov-render-document run-mode-hooks nov-mode-hook nov-mode-abbrev-table local-abbrev-table buffer-file-name nov-temp-dir exit-code content content-file work-dir nov-content-file nov-epub-version nov-metadata nov-documents nov-documents-index buffer-undo-list] 6 (#$ . 19094) nil])
(provide 'nov)
